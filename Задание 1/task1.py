# Общая сложность по времени: O(n log n) в среднем и O(n^2) в худшем случае.
# По памяти: O(log n), а в худшем случае - O(n).

def quicksort(array: list[int]) -> list[int]:
    if len(array) <= 1:
        return array

    support = array[len(array) // 2]
    left = [x for x in array if x < support]
    equal = [x for x in array if x == support]
    right = [x for x in array if x > support]
    return quicksort(left) + equal + quicksort(right)


if __name__ == "__main__":
    example = [1, 0, 3, 5, 1, 4, 9, 4, 24, -7, 2]
    print(quicksort(example))
