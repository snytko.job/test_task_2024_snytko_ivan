import re
from collections import Counter


def count_word_frequencies(filename):
    # Открываем и читаем файл
    with open(filename, 'r', encoding='utf-8') as file:
        text = file.read()

    # Преобразуем текст к нижнему регистру и используем регулярное выражение для извлечения слов
    words = re.findall(r'\b\w+\b', text.lower())

    # Подсчитываем частоту каждого слова
    word_counts = Counter(words)

    # Сортируем слова по частоте в убывающем порядке
    sorted_word_counts = word_counts.most_common()

    # Выводим результаты
    for word, count in sorted_word_counts:
        print(f'{word}: {count}')


# Пример использования
filename = 'example.txt'
count_word_frequencies(filename)
