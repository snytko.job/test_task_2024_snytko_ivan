def binarySearch(array: list[int], element: int) -> int:
    start = 0
    stop = len(array) - 1

    while start <= stop:
        middle = (start + stop) // 2

        if array[middle] == element:
            return middle
        else:
            if array[middle] > element:
                stop = middle - 1
            else:
                start = middle + 1

    return -1


if __name__ == "__main__":
    massiv = [i for i in range(1, 101, 2)]
    print(massiv)
    el = 99
    print(binarySearch(massiv, el))
