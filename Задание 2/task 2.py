# Задумка такая: проверить кол-во вхождений элементов А, на основе элементов В сформировать новый список с кол-вом из
# А, отсекая найденные в А значения В. Далее, в словаре, остаются значения, не входящие в В → их сортируем по убыванию.
# Общая сложность по времени: O(n logn), по памяти: O(n), n - кол-во элементов в А

def sortAlikeB(a: list[int], b: list[int]) -> list[int]:
    entries = {}
    result1 = []

    for element in a:
        if element in entries:
            entries[element] += 1
        else:
            entries[element] = 1

    for element in b:
        for i in range(entries[element]):
            result1.append(element)

        del entries[element]

    result2 = []

    for key in entries.keys():
        for j in range(entries[key]):
            result2.append(key)

    result2 = quicksortDescending(result2)

    return result1 + result2


def quicksortDescending(array: list[int]) -> list[int]:
    if len(array) <= 1:
        return array

    support = array[len(array) // 2]
    left = [x for x in array if x > support]
    equal = [x for x in array if x == support]
    right = [x for x in array if x < support]
    return quicksortDescending(left) + equal + quicksortDescending(right)


if __name__ == "__main__":
    A = [2, 4, 1, 3, 2, 4, 6, 7, 9, 2, 19]
    B = [2, 1, 4, 3, 6, 9]
    print(sortAlikeB(A, B))
